import * as firebase from 'firebase'
import { Alert } from 'react-native'
import {getAuth, createUserWithEmailAndPassword} from "firebase/auth";

export async function registration(Username,Email,Passsword) {
    try {
        await firebase.auth().createUserWithEmailAndPassword(Email, Passsword);
        const currentUser  = firebase.auth().currentUser;

        const db = firebase.firestore
        db.collection('user').doc(currentUser.uid).set({
            email:Email,
            password:Passsword,
        })
    } catch (error) {
        Alert.alert("Oops Something is wrong!!!", error.message);
    }
    
}