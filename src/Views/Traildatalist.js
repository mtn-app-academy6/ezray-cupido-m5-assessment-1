import Locate1 from '../../assets/Locate.png'
import Locate2 from '../../assets/Locate.png'
import Locate3 from '../../assets/Locate.png'
import Locate4 from '../../assets/Locate.png'
// <a href="https://www.flaticon.com/free-icons/location" title="location icons">Location icons created by IconMarketPK - Flaticon</a>

const Traildatalist= [
    {
        trail:"Pipe Track",
        level:"Beginner",
        review:'4',
        Image:Locate1,
        coord:{latitude:-33.927109,longitude:18.420059}
        
    },

    {
        trail:"Elephants Eye",
        level:"Beginner",
        review:'3',
        Image:Locate2,
        coord:{latitude:-33.939110,longitude:18.420060}

     
    },

    {
        trail:"Lions Head",
        level:"Beginner",
        review:'5',
        Image:Locate3,
        coord:{latitude:-33.936097,longitude:18.389222}

        
    },

    {
        trail:"Constantia Nek",
        level:"Beginner",
        review:'2',
        Image:Locate4,
        coord:{latitude:-34.021900,longitude:18.432660}


    }, ]

    export default Traildatalist;