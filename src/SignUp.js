//import liraries
import React, { Component, useState } from 'react';
import { View, Dimensions, Text, StyleSheet,SafeAreaView,TextInput,TouchableOpacity} from 'react-native';
import { registration } from './Backend/Authentication';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

// create a component
const SignUp = ({navigation}) => {

  const[Username, setUsername] = useState('')
  const[Email, setEmail] = useState('')
  const[Password, setPassword] = useState('')
  const signupUser=()=>{
      registration(Username,Password,Email)
  }

    return (
        <SafeAreaView style={styles.container}>
        <Text style={{fontWeight: '100', fontSize: 60, width:900, marginBottom: 10,}}>HikeClub</Text>
        <Text style={{ marginBottom: 10, fontSize: 20 ,}}>
            Sign up for Adventure
            </Text>
            <View style={styles.form}>
            <TextInput
          style={styles.input}
          placeholder="Username"
          value={Username}
          name='Username'
          onChange={(Username)=>{setUsername(Username)}}
        
        />
            <TextInput
          style={styles.input}
          placeholder="Email"
          value={Email}
          name='Email'
          onChange={(Email)=>{setEmail(Email)}}
        
        />
        <TextInput
          style={styles.input}
          placeholder="Passsword"
          value={Password}
          name='Password'
          onChange={(Passsword)=>{setPassword(Passsword)}}
          
        />
            </View>
       
         <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate('Home')}
      >
        <Text style={{color:'white'}}>Sign In</Text>
      </TouchableOpacity>
      </SafeAreaView>
    );
};

// define your styles
const styles = StyleSheet.create({
    input: {
        height: 40,
        margin: 12,
//borderWidth: 1,
        borderBottomWidth:1,
        padding: 10,
      },
      button: {
        alignItems: "center",
        backgroundColor: "#ef6c00",
        padding: 10,
        margin:10,
        borderRadius: 10
    
      },
      form:{
        margin:16,
        paddingTop:windowHeight/6 
      }
});

//make this component available to the app
export default SignUp;
